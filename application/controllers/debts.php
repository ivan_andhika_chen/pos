<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Debts extends CI_Controller {

  public function index()
  {
    $data['transactions'] = $this->transaction->find_by(array('date >' => date('Y-m-d 00:00:00', strtotime('now')), 'date <=' => date('Y-m-d 23:59:59', strtotime('now')), 'paid' => 0));
    $this->load->view('templates/layout',array_merge($data, get_template('Utang','transactions/index')));
  }

  public function details($id)
  {
    $data['transaction_details'] = $this->transaction_detail->find_by(array('id' => $id));
    $this->load->view('templates/layout',array_merge($data, get_template('Utang','transactions/show')));
  }
}

/* End of file debts.php */
/* Location: ./application/controllers/debts.php */