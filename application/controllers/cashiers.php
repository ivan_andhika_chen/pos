<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cashiers extends CI_Controller {

  public function index(){
    $this->load->view('templates/layout',array_merge(array(),get_template('Cashier','cashiers/index')));
  }

  public function add_item(){
    $status = '';
    $barcode = trim($this->input->post('barcode'));
    $value = explode("*",$barcode);
    if(empty($value[1])){
      $data['item'] = $this->item->get_item_by_barcode(trim($value[0]));
      if(!empty($data['item'])){
        if($data['item']->real_stock > 0){
          $data['total_item'] = 1;
          $status = 'add_item';
        }
        else{
          $status = 'stock_error';
        }
      }
    }
    else{
      $data['item'] = $this->item->get_item_by_barcode(trim($value[1]));
      if(!empty($data['item'])){
        if($data['item']->real_stock >= $value[0]){
          $data['total_item'] = $value[0];
          $status = 'add_item';
        }
        else{
          $status = 'stock_error';
        }
      }
    }

    if($this->input->post('ajax') && $status == 'add_item'){
      if(!$this->input->post('present_item')){
        $this->load->view('cashiers/item', $data);
      }
      else{
        echo 'add_present_item';
      }
    }
    else if($this->input->post('ajax') && $status == 'stock_error'){
      echo 'stock error';
    }
    else{
      echo 'no data';
    }
  }

  public function save_item(){
    $status = false;
    $items = $this->input->post('item');
    $datas = Array();
    $quantities = Array();
    $grand_total = 0;
    $index = 0;
    foreach($items as $item){
      $temp = $this->item->get_item_by_barcode($item['barcode']);
      $total_item = $item['qty'];
      if(!empty($temp)){
        if($total_item > $temp->stock){
          $total_item = $temp->stock;
        }
        $datas[$index] = $temp;
        $quantities[$index] = $total_item;
        $grand_total += $total_item * $temp->selling_price;
        $index++;
      }
    }

    $user_id = $this->session->userdata('id');
    $transaction_number = $this->generate_transaction_number($user_id);
    if(!empty($datas)){
      $this->transaction->insert_transaction($transaction_number, $user_id, '', $this->input->post('type_id'), $grand_total);
      $transaction_id = $this->db->insert_id();

      $index = 0;
      foreach($datas as $data){
        $stock = $data->stock - $quantities[$index];
        $total = $quantities[$index] * $data->selling_price;
        $this->transaction_detail->insert_transaction_detail($transaction_id, $data->id, $quantities[$index], $data->selling_price, $total);
        $this->item->update_item_by_barcode($data->barcode, $stock);
        $index++;
      }
      $status = true;
    }

    if($this->input->post('ajax') && $status){
      echo 'complete';
    }
  }

  private function generate_transaction_number($user_id){
    $transaction_number = $user_id . '_' . date('ymd_His', strtotime('now'));
    return $transaction_number;
  }
}

/* End of file cashiers.php */
/* Location: ./application/controllers/cashiers.php */
