<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboards extends CI_Controller {

  function index(){
    if (!is_logged_in ()){
      redirect('/dashboards/login');
    }
    $data['notification']='';
    $this->load->view('templates/layout', array_merge($data,get_template('Beranda','dashboards/index')));
  }

  function login(){
    $data['content_class']="login";
    $data['is_login_page']=true;
    $this->load->view('templates/layout', array_merge($data,get_template('Login Page','dashboards/login')));
  }

  function validate(){
    if ($this->user->validate($this->input->post('username'),$this->input->post('password'))){
        $logged_in = $this->user->find_by(array('username'=> $this->input->post('username'),'password'=>md5($this->input->post('password'))));
          $data = array(
            'username' => $logged_in['0']->username,
            'name' => $logged_in['0']->name,
            'id' => $logged_in['0']->id,
            'is_logged_in' => true
          );
          $this->session->set_flashdata('notification', array('type'=>'success','message'=>'Selamat Datang, '.$logged_in['0']->name));
          $this->session->set_userdata($data);
          redirect('/dashboards/index');

    }else{
      $this->session->set_flashdata('notification',array('type'=>'error','message'=>'Username dan/atau password Anda, salah.'));
      redirect('/dashboards/login');
    }
  }

  function logout() {
    $this->session->sess_destroy();
    redirect('/dashboards/login');
  }

}

/* End of file dashboards.php */
/* Location: ./application/controllers/dashboards.php */