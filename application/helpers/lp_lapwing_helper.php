<?php

function is_logged_in() {
    $CI = & get_instance();
    $is_logged = $CI->session->userdata('is_logged_in');
    if (!isset($is_logged) || $is_logged != true) {
        return false;
    } else {
        return true;
    }
}

function current_user(){
    $CI = & get_instance();
    $id = $CI->session->userdata('id');
    if (isset($id)){
        // $user = $CI->karyawan->find_by(array('id' => $id),"one");
        return "$user";
    }else{
        return "";
    }
}

function current_date(){
    $date = getDate();
    return $date['year']."-".$date['mon']."-".$date['mday'];
}

function current_month(){
    $date = getDate();
    return $date['mon'];
}

function to_yyyymmdd($ddmmyyyy){
    return substr($ddmmyyyy,6,4)."-".substr($ddmmyyyy,3,2)."-".substr($ddmmyyyy,0,2);
}

function get_template($location,$main_contents){
  $CI = & get_instance();
  $notification = $CI->session->flashdata('notification');
  if (isset($notification)){
      $data['notification_type']=$notification['type'];
      $data['notification']=$notification['message'];
  }else{
      $data['notification_type']="info";
      $data['notification']="no notification";
  }

  $data['months'] = get_months();
  $data['years'] = get_years(5);
  $data['this_month'] = this_month();
  $data['this_year'] = this_year();
  $data['location']=$location;
  $data['main_contents']=$main_contents;
  return $data;
}


function get_months(){
  $months = array(
    array('name'=> 'Januari','id'=>'1'),
    array('name'=> 'Februari','id'=>'2'),
    array('name'=> 'Maret','id'=>'3'),
    array('name'=> 'April','id'=>'4'),
    array('name'=> 'Mei','id'=>'5'),
    array('name'=> 'Juni','id'=>'6'),
    array('name'=> 'Juli','id'=>'7'),
    array('name'=> 'Agustus','id'=>'8'),
    array('name'=> 'September','id'=>'9'),
    array('name'=> 'Oktober','id'=>'10'),
    array('name'=> 'November','id'=>'11'),
    array('name'=> 'December','id'=>'12')
  );
  return $months;
}

function get_years($range){
  $getdate = getdate();
  $year= intVal($getdate['year'])-$range;
  $years = array();
  for ($i=0; $i <($range*2) ; $i++) { 
    $years[$i]= $year+$i;
  }
  return $years;
}

function this_month(){
  $getdate = getdate();
  return $getdate['mon'];
}

function this_year(){
  $getdate = getdate();
  return $getdate['year'];
}