<!DOCTYPE html>
<html lang="en">
<head>
<title>404 Page Not Found</title>
<style type="text/css">

::selection{ background-color: #E13300; color: white; }
::moz-selection{ background-color: #E13300; color: white; }
::webkit-selection{ background-color: #E13300; color: white; }

body {
	background: url('/webroot/imgs/474E56.png');
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #FFFFFF;
}

a {
	color: #FFFFFF;
	background-color: transparent;
	font-weight: normal;
}

h1,h2 {
	background-color: transparent;
	/*border-bottom: 1px solid #D0D0D0;*/
	font-weight: normal;
	/*margin: 10px;*/
	/*padding: 14px 15px 10px 15px;*/
	text-align: center;
}

h1{
	color: #79BDEA;
	font-size: 99px;
	text-shadow: 0px 0px 13px #0C629D;
}

h2{
	color: #CCCCCC;
	font-size: 77px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	/*border: 1px solid #D0D0D0;*/
	color: #FFFFFF;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin: 10px;
	/*border: 1px solid #D0D0D0;*/
	-webkit-box-shadow: 0 0 8px #D0D0D0;
}

p {
	margin: 12px 15px 12px 15px;
}
</style>
</head>
<body>
	<div id="container">
		<h1><?php echo $heading; ?></h1>
		<h2><?php echo $message; ?></h2>
		<h1 style="font-size: 500px;">?</h1>
	</div>
</body>
</html>