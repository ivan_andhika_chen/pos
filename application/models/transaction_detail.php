<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction_detail extends CI_Model {

  // var $quantity = 0;
  // var $unit_price = 0;
  // var $total_price = 0;

  function get_all_transaction_details()
  {
    $query = $this->db->get('transaction_details');
    return $query->result();
  }

  function find_by($params,$one_or_many=null){
    $query = $this->db->get_where('transaction_details', $params);
    if ($one_or_many=="one"){
      return $query->row();
    }else{
      return $query->result();
    }
  }

  function find_column_by($column,$params){
    $this->db->select($column);
    $query = $this->db->get_where('transaction_details', $params);
    return $query->row();
  }

  function insert_transaction_detail($transaction_id, $item_id, $quantity, $unit_price, $total_price){
    $data = array(
      'transaction_id' => $transaction_id,
      'item_id' => $item_id,
      'quantity' => $quantity,
      'unit_price' => $unit_price,
      'total_price' => $total_price,
      'created_at' => date('Y-m-d H:i:s', strtotime('now')),
      'updated_at' => date('Y-m-d H:i:s', strtotime('now'))
    );
    $this->db->insert('transaction_details', $data);
  }
}

/* End of file transaction_detail.php */
/* Location: ./application/models/transaction_detail.php */