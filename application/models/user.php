<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Model {

  // var $name = '';
  // var $username = '';
  // var $password = '';

  function get_all_users(){
    $query = $this->db->get('users');
    return $query->result();
  }

  function find_by($params,$one_or_many=null){
    $query = $this->db->get_where('users', $params);
    if ($one_or_many=="one"){
      return $query->row();
    }else{
      return $query->result();
    }
  }

  function find_column_by($column,$params){
    $this->db->select($column);
    $query = $this->db->get_where('users', $params);
    return $query->row();
  }

  function validate($username,$password){
    $username = $this->input->post('username');
    $password = md5($this->input->post('password'));
    $this->db->from('users');
    $this->db->where('username', $username);
    $this->db->where('password', $password);
    $count = $this->db->count_all_results();
    if ($count > 0){ return true; }else{ return false; }
  }

}

/* End of file user.php */
/* Location: ./application/models/user.php */