<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item extends CI_Model {

  // var $barcode = '';
  // var $name = '';
  // var $description = '';
  // var $vendor_unit_price = 0;
  // var $ready_stock = 0;
  // var $selling_price = 0;

  function __construct(){
    parent::__construct();
  }

  function get_all_items(){
    $query = $this->db->get('items');
    return $query->result();
  }

  #@author Albert
  #Method ini digunakan untuk mencari barcode
  #return int
  function get_item_by_barcode($barcode){
    $query = $this->db->get_where('items', array('barcode' => $barcode));
    return $query->row();
  }

  function get_item_by_id($id){
    $query = $this->db->get_where('items', array('id' => $id));
    return $query->row();
  }

  function update_item_by_barcode($barcode, $stock){
    $data = array('stock' => $stock, 'real_stock' => $stock);
    $this->db->where('barcode', $barcode);
    $this->db->update('items', $data); 
  }
}

/* End of file item.php */
/* Location: ./application/models/item.php */