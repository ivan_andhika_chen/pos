<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction extends CI_Model {

  // var $number = '';
  // var $payment_date = '';
  // var $paid = 0;
  // var $grand_total = 0;

  function find_by($params,$one_or_many=null){
    $query = $this->db->get_where('transactions', $params);
    if ($one_or_many=="one"){
      return $query->row();
    }else{
      return $query->result();
    }
  }

  function find_column_by($column,$params){
    $this->db->select($column);
    $query = $this->db->get_where('transactions', $params);
    return $query->row();
  }

  function insert_transaction($number, $employee_id, $customer_id, $paid, $grand_total){
    $data = array(
      'number' => $number,
      'date' => date('Y-m-d H:i:s', strtotime('now')),
      'employee_id' => $employee_id,
      'customer_id' => $customer_id,
      'paid' => $paid,
      'payment_date' => date('Y-m-d H:i:s', strtotime('now')),
      'grand_total' => $grand_total,
      'created_at' => date('Y-m-d H:i:s', strtotime('now')),
      'updated_at'=> date('Y-m-d H:i:s', strtotime('now'))
    );
    $this->db->insert('transactions', $data);
  }
}

/* End of file transaction.php */
/* Location: ./application/models/transaction.php */