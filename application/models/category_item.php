<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_item extends CI_Model {

  var $name = '';

  function __construct()
  {
    parent::__construct();
  }

  function get_all_category_items()
  {
    $query = $this->db->get('category_items');
    return $query->result();
  }

  function get_category_item_by_id()
  {
    $query = $this->db->get('entries');
    return $query->result();
  }
}

/* End of file category_item.php */
/* Location: ./application/models/category_item.php */