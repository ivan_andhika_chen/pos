<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendor extends CI_Model {

  var $name = '';
  var $contact_person = '';
  var $address = '';
  var $phone = '';

  function __construct()
  {
    parent::__construct();
  }

  function get_all_vendors()
  {
    $query = $this->db->get('vendors');
    return $query->result();
  }
}

/* End of file vendor.php */
/* Location: ./application/models/vendor.php */