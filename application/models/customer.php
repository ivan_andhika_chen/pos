<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Model {

  var $identity_number = 0;
  var $name = '';
  var $address = '';
  var $phone = '';

  function __construct()
  {
    parent::__construct();
  }

  function get_all_customers()
  {
    $query = $this->db->get('customer');
    return $query->result();
  }
}

/* End of file customer.php */
/* Location: ./application/models/customer.php */