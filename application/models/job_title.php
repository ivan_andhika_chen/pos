<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Job_title extends CI_Model {

  var $title = '';

  function __construct()
  {
    parent::__construct();
  }

  function get_all_job_titles()
  {
    $query = $this->db->get('job_titles');
    return $query->result();
  }
}

/* End of file job_title.php */
/* Location: ./application/models/job_title.php */