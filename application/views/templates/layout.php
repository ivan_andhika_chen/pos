<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><!--DVID=00042DD1-->
<html lang="en" dir="ltr">
<?php include 'head.php'; ?>
<body>
  <div id="viewport">
    <?php include 'header.php'; ?>
    <div id="main_content">
      <div id="content" class="<?= isset($content_class) ? $content_class : ""; ?>">
        <?php $this->load->view( $main_contents ); ?>
      </div>
    </div>
    <?php include 'footer.php'; ?>
  </div>
</body>
<?php include 'script.js.php'; ?>
</html>