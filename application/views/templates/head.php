<head>
  <link rel="stylesheet" type="text/css" href="/webroot/css/dashboard_template.css" />
  <link href="/webroot/css/jquery-ui-1.9.2.custom.css" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" href="/webroot/css/notification.css" />

  <script src="/webroot/js/jquery-1.8.3.js"></script>
  <script src="/webroot/js/jquery-ui-1.9.2.custom.js"></script>
  <script src="/webroot/js/lapwing.js"></script>
  <script src="/webroot/js/jquery.validation.js"></script>
  <script src="/webroot/js/form_autocomplete.js"></script>
  <title>Lapwing POS</title>
</head>