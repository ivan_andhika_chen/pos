<footer>
  <div>
    <div class="left">
      <ul class="menu">
        <li>
          <a href="#">Report Problem</a>
        </li>
        <li class="end">
          <a href="#">About</a>
        </li>
      </ul>
    </div>
    <div class="right">
      <ul>
        <li class="end">
          <p>created by </p><a class="lapwing" href="#" title="Go to Lapwing Project Home Page">LapwingProject</a>
        </li>
      </ul>
    </div>
    <br class="clear"/>
  </div>
</footer>