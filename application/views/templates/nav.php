<nav>
    <ul class="left" style="width: 600px;">
      <?php if ( !isset($is_login_page)) : ?>
      <li>
        <a href='<?=base_url()?>dashboards/'>Dashboard</a>
      </li>
      <li>
        <a href="<?=base_url()?>cashiers/">Cashier</a>
      </li>
      <li>
        <a href="<?=base_url() ?>#">Transaksi</a>
        <ul>
          <li ><a href='<?=base_url() ?>debts/' >Utang</a>
          </li>
          <li><a href="<?=base_url() ?>transactions/">Penjualan</a></li>
        </ul>
      </li>
    </ul>

    <ul class="right">
      <li><a href="#">Setting</a>
        <ul>
          <li><a href="<?=base_url() ?>configurations/">Konfigurasi</a></li>
          <li><a href="<?=base_url() ?>helps/">Pertolongan</a></li>
          <li class="separator"><a href="<?=base_url() ?>feedbacks/">Feedback</a></li>
        </ul>
      </li>
      <li><a href="<?=base_url()."dashboards/logout"?>">Keluar</a></li>
      <?php endif; ?>
    </ul>
</nav>