<div class="info message">
  <h3>Informasi</h3>
  <p><?=$notification;?></p>
</div>

<div class="error message">
  <h3>Kesalahan Teknis</h3>
  <p><?=$notification;?></p>
</div>

<div class="warning message">
  <h3>Peringatan!</h3>
  <p><?=$notification;?></p>
</div>

<div class="success message">
  <h3>Sukses</h3>
  <p><?=$notification;?></p>
</div>