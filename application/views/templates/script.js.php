<script>
  $(document).ready(function(){
    relayout();
    <?php if (isset($notification_type)): ?>
    showMessage('<?=$notification_type;?>');
    <?php endif; ?>
    $('.message').click(function(){
        $(this).animate({top: -$(this).outerHeight()}, 500);
    });
  });

  function relayout(){
    var documentHeight = parseInt($(window).height());
    var contentHeight = documentHeight-116
    // console.log(documentHeight);
    // console.log(contentHeight);
    $("#main_content").height(contentHeight);
    $("#content").height((contentHeight-90));
  }
  $(window).resize(function(){
    relayout();
  });
</script>