<table class="blue" style="">
  <thead>
    <tr>
      <th class="w10">Number</th>
      <th class="w10">Date</th>
      <th class="w10">Employee id</th>
      <th class="w10">Payment date</th>
      <th class="w10">Grand total</th>
      <th class="w10"></th>
    </tr>
  </thead>
  <tbody id="list_transactions">
    <?php foreach($transactions as $transaction): ?>
      <tr class='odd'>
        <td><?= $transaction->number; ?></td>
        <td><?= $transaction->date; ?></td>
        <td><?= $transaction->employee_id; ?></td>
        <td><?= $transaction->payment_date; ?></td>
        <td><?= $transaction->grand_total; ?></td>
        <td><?= anchor('debts/details/'.$transaction->id, 'Details'); ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  <tfoot id="total_list_receipts" class="total">
    <tr>
      <td></td>
    </tr>
  </tfoot>
</table>