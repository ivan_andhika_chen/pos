<table class="blue" style="">
  <thead>
    <tr>
      <th class="w10">Item id</th>
      <th class="w10">Quantity</th>
      <th class="w10">Unit Price</th>
      <th class="w10">Total Price</th>
    </tr>
  </thead>
  <tbody id="list_transaction_details">
    <?php foreach($transaction_details as $transaction_detail): ?>
      <tr class='odd'>
        <td><?= $this->item->get_item_by_id($transaction_detail->item_id)->name; ?></td>
        <td><?= $transaction_detail->quantity; ?></td>
        <td><?= $transaction_detail->unit_price; ?></td>
        <td><?= $transaction_detail->total_price; ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  <tfoot id="total_list_receipts" class="total">
    <tr>
      <td></td>
    </tr>
  </tfoot>
</table>