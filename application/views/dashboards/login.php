<?=form_open('/dashboards/validate', array('id' => 'form_login'));?>
<?= form_label('Username', 'username')?>
<?= form_input(array('name'=>'username', 'id' => 'username'));?>
<br/>
<?= form_label('Password', 'password')?>
<?= form_password(array('name'=>'password', 'id'=>'password'));?>
<br/>
<?= form_label('&nbsp;', 'login_button')?>
<?= form_submit('login_button', 'Login');?>

<script type="text/javascript">
$(document).ready(function(){
    var name = document.URL.split('#')
    if (name.length>1){
    window.location = document.URL.split('#')[0];
    }
  });
</script>