<div class="panel_filter">
  <div id="action" class="right">
    <a class="button" href="#">Print and Save</a>
  </div>
  <div id="action" class="right">
    <a class="button" id="save-utang" href="#">Save Utang</a>
  </div>
  <div id="action" class="right">
    <a class="button" id="save-only" href="#">Save Only</a>
  </div>
  <br class="clear"/>
</div>
<div class="box" style="width:40%; float: left;">
  <?=form_label('barcode', 'barcode'); ?>
  <?=form_input(array('name'=>'barcode', 'id'=>'barcode', 'class'=>'ac_barcode')); ?>
  <span class="notification" style="display:none;color:red;">Wrong input!</span>
</div>
<div class="box" style="width:40%; float: right;">
  <?=form_label('Subtotal', 'subtotal'); ?><?=form_input(array('name'=>'subtotal', 'id'=>'subtotal', 'class'=>'money', 'readonly'=> true)); ?><br/>
  <?=form_label('Tax', 'tax'); ?><?=form_input(array('name'=>'tax', 'id'=>'tax', 'class'=>'money', 'value' => 0)); ?><span>%</span>
  <hr>
  <?=form_label('Total', 'total'); ?><?=form_input(array('name'=>'total', 'id'=>'total', 'class'=>'money', 'readonly'=> true)); ?><br/>
  <?=form_label('Payment', 'payment'); ?><?=form_input(array('name'=>'payment', 'id'=>'payment', 'class'=>'money')); ?>
  <hr>
  <?=form_label('Change', 'change'); ?><?=form_input(array('name'=>'change', 'id'=>'change', 'class'=>'money', 'readonly'=> true)); ?>
</div>

<br class="clear"/>
<br>
<table class="blue" style="">
  <thead>
    <tr>
      <th class="w10">No.</th>
      <th class="w200">Nama</th>
      <th class="w50">Qty</th>
      <th class="w100">Satuan</th>
      <th class="end w100">Sub Total</th>
    </tr>
  </thead>
  <tbody id="list_receipts">
    <tr class="odd">
      <?= form_hidden('list_receipts[1][barcode]', '');?>
      <td class="no right-text">
        <?= form_input('list_receipts[1][no]', '1', "class='no' id='list_receipts_1_no' readonly"); ?>
      </td>
      <td class="name">
        <?= form_input('list_receipts[1][name]', '', "class='name' id='list_receipts_1_name' readonly"); ?>
      </td>
      <td class="qty right-text">
        <?= form_input('list_receipts[1][qty]', '', "class='qty' id='list_receipts_1_qty' readonly"); ?>
      </td>
      <td class="unit_price right-text">
        <?= form_input('list_receipts[1][unit_price]', '', "class='unit_price' id='list_receipts_1_unit_price' readonly"); ?>
      </td>
      <td class="subtotal right-text">
        <?= form_input('list_receipts[1][subtotal]', '', "class='subtotal' id='list_receipts_1_subtotal' readonly"); ?>
      </td>
    </tr>
  </tbody>
  <tfoot id="total_list_receipts" class="total">
    <tr>
      <td colspan="4" class="right-text">Total</td>
      <td id="total_receipt" class="right-text"><?= form_input('total_receipt', '', "class='right-text'"); ?></td>
    </tr>
  </tfoot>
</table>
<?php include 'index.js.php' ?>