<tr class="odd <?= $item->barcode ?>">
  <?= form_hidden('list_receipts[1][barcode]', $item->barcode);?>
  <td class="no right-text">
    <?= form_input('list_receipts[1][no]', '1', "class='no' id='list_receipts_1_no' readonly"); ?>
  </td>
  <td class="name">
    <?= form_input('list_receipts[1][name]', $item->name, "class='name' id='list_receipts_1_name' readonly"); ?>
  </td>
  <td class="qty right-text">
    <?= form_input('list_receipts[1][qty]', $total_item, "class='qty' id='list_receipts_1_qty' readonly"); ?>
  </td>
  <td class="unit_price right-text">
    <?= form_input('list_receipts[1][unit_price]', $item->selling_price * 1, "class='unit_price' id='list_receipts_1_unit_price' readonly"); ?>
  </td>
  <td class="subtotal right-text">
    <?= form_input('list_receipts[1][subtotal]', $total_item * $item->selling_price, "class='subtotal' id='list_receipts_1_subtotal' readonly"); ?>
  </td>
</tr>