<script type="text/javascript">
  $(document).ready(function(){

    $("#save-only").click(function(){
      var i = 0;
      var item = [];

      $("#list_receipts > tr").each(function(){
        var value = {};
        value['barcode'] = $(this).children("input").first().val();
        value['qty'] = $(this).children(".qty").children("input").val();
        item[i] = value;
        i++;
      });

      var form_data = {
        item: item,
        ajax: '1',
        type_id: '1'
      }

      $.ajax({
        url: "<?= site_url('cashiers/save_item'); ?>",
        type: 'POST',
        data: form_data,
        success: function(msg){
          if (msg == "complete"){
            window.location = "<?=base_url() ?>cashiers";
          }
          else{
            showMessageWithText('error', 'Transaksi gagal disimpan.');
          }
        }
      });
      return false;
    });

    $("#save-utang").click(function(){
      var i = 0;
      var item = [];

      $("#list_receipts > tr").each(function(){
        var value = {};
        value['barcode'] = $(this).children("input").first().val();
        value['qty'] = $(this).children(".qty").children("input").val();
        item[i] = value;
        i++;
      });

      var form_data = {
        item: item,
        ajax: '1',
        type_id: '0'
      }

      $.ajax({
        url: "<?= site_url('cashiers/save_item'); ?>",
        type: 'POST',
        data: form_data,
        success: function(msg){
          if (msg == "complete"){
            window.location = "<?=base_url() ?>cashiers";
          }
          else{
            showMessageWithText('error', 'Transaksi gagal disimpan.');
          }
        }
      });
      return false;
    });

    $("#barcode").keyup(function(event){
      if (event && event.which == 13) {
        barcode = $('#barcode').val().split('*');
        item_present = 0

        event.preventDefault();
        var form_data = {
          barcode: $('#barcode').val(),
          ajax: '1'
        }

        if(barcode.length == 1){
          if($('.'+barcode[0]).length){
            form_data['present_item'] = 1;
          }
          $.ajax({
            url: "<?= site_url('cashiers/add_item'); ?>",
            type: 'POST',
            data: form_data,
            success: function(msg){
              if(msg == "no data"){
                barcode_error_notification('Item belum tersedia!');
              }
              else if(msg == 'stock error'){
                barcode_error_notification('Total barang tersedia!');
              }
              else if(msg == 'add_present_item'){
                temp_total = $('.'+barcode[0]).children(".qty").children("input").val();
                $('.'+barcode[0]).children(".qty").children("input").val(+temp_total + 1);
                $('#barcode').val('');
              }
              else{
                $('.notification').hide();
                if($("#list_receipts > tr > td.name").children().val() == ''){
                  $('#list_receipts').html(msg);
                }
                else{
                  $('#list_receipts').append(msg);
                }
                $('#barcode').val('');
                reindex();
              }
            }
          }); // end ajax
        }
        else if(barcode.length == 2){
          if(barcode[0] == 0){
            if($('.'+barcode[1]).length){
              if($("#list_receipts > tr").length > 1){
                $('.'+barcode[1]).fadeOut(400, function(){
                  $(this).remove();
                  reindex();
                });
              }
              else{
                $("#list_receipts > tr > td").children().val('');
                $("#list_receipts > tr").removeClass(barcode[1]);
                reindex();
              }
            }
            else{
              barcode_error_notification('Item belum tersedia');
            }
            $('#barcode').val('');
          }
          else if(barcode[0] < 0){
            if($('.'+barcode[1]).length){
              pengurang = +(barcode[0].replace('-',''));
              total = +($('.'+barcode[1]).children(".qty").children("input").val());
              if(pengurang < total){
                $('.'+barcode[1]).children(".qty").children("input").val(total - pengurang);
                $('#barcode').val('');
                $('.notification').hide();
              }
              else{
                barcode_error_notification('Input pengurang salah!');
              }
            }
            else{
              barcode_error_notification('Item belum tersedia');
            }
          }
          else if(barcode[0] > 0){
            if($('.'+barcode[1]).length){
              form_data['present_item'] = 1;
            }
            $.ajax({
              url: "<?= site_url('cashiers/add_item'); ?>",
              type: 'POST',
              data: form_data,
              success: function(msg){
                if(msg == "no data"){
                  barcode_error_notification('Item belum tersedia!');
                }
                else if(msg == 'stock error'){
                  barcode_error_notification('Total barang tidak tersedia!');
                }
                else if(msg == 'add_present_item'){
                  temp_total = $('.'+barcode[1]).children(".qty").children("input").val();
                  $('.'+barcode[1]).children(".qty").children("input").val(+temp_total + +barcode[0]);
                  $('#barcode').val('');
                }
                else{
                  $('.notification').hide();
                  if($("#list_receipts > tr > td.name").children().val() == ''){
                    $('#list_receipts').html(msg);
                  }
                  else{
                    $('#list_receipts').append(msg);
                  }
                  $('#barcode').val('');
                  reindex();
                }
              }
            }); // end ajax
          }
        }
        else{
          barcode_error_notification('Input salah!');
        }
        return false;
      }
    });

    $("#tax").keyup(function(event){
      if (event) {
        event.preventDefault();
        countTotal();
        countChange();
      }
    });

    $("#payment").keyup(function(event){
      if (event) {
        event.preventDefault();
        countChange();
      }
    });
  });

  function reindex(){
    var index = 1;
    $("#list_receipts > tr").each(function(){
      if (index%2 == 1) {
        $(this).removeClass('odd');
        $(this).removeClass('even');
        $(this).addClass('odd');
      } else{
        $(this).removeClass('odd');
        $(this).removeClass('even');
        $(this).addClass('even');
      };
      $(this).children(".no").children("input").val(index);
      $(this).children("input").attr('name','list_receipts['+index+'][barcode]');
      $(this).children(".no").children("input").attr('name','list_receipts['+index+'][no]');
      $(this).children(".name").children("input").attr('name','list_receipts['+index+'][name]');
      $(this).children(".qty").children("input").attr('name','list_receipts['+index+'][qty]');
      $(this).children(".unit_price").children("input").attr('name','list_receipts['+index+'][unit_price]');
      $(this).children(".subtotal").children("input").attr('name','list_receipts['+index+'][subtotal]');

      $(this).children(".no").children("input").attr('id','list_receipts_'+index+'_no');
      $(this).children(".name").children("input").attr('id','list_receipts_'+index+'_name');
      $(this).children(".qty").children("input").attr('id','list_receipts_'+index+'_qty');
      $(this).children(".unit_price").children("input").attr('id','list_receipts_'+index+'_unit_price');
      $(this).children(".subtotal").children("input").attr('id','list_receipts_'+index+'_subtotal');
      index++;
    })
    countSubTotal();
  }

  function countSubTotal(){
    var total=0;
    $("#list_receipts > tr").each(function(){
      total += +($(this).children(".subtotal").children("input").val());
    });
    $("#total_receipt").children("input").val(total);
    $("#subtotal").val(total);
    countTotal();
    countChange();
  }

  function countTotal(){
    var subtotal = +($("#subtotal").val());
    var tax = +($("#tax").val());
    var total = subtotal + (tax / 100 * subtotal);
    $("#total").val(total);
  }

  function countChange(){
    var total = +($('#total').val());
    var payment = +($('#payment').val());
    $('#change').val(payment - total);
  }

  function barcode_error_notification(message){
    $('.notification').text(message).show();
  }
</script>