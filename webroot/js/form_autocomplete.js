function autocomplete() {
  function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
  }

  $(".ac_uang_muka").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "http://"+window.location.hostname+"/index.php/autocomplete/penjualan",
        dataType: "json",
        data: {
          maxRows: 12,
          term: request.term
        },
        success: function( data ) {
          response( $.map( data.result, function( item ) {
            return {
                label: item.no_ref + " - "+ item.no_rangka + " - " + item.nama_konsumen,
                value: item.no_ref,
                no_ref: item.no_ref,
                no_rangka: item.no_rangka,
                no_mesin: item.no_mesin,
                no_serie: item.no_serie,
                no_stnk: item.no_stnk,
                kendaraan_id: item.kendaraan_id,
                tipe: item.tipe,
                warna: item.warna,
                uang_muka: item.uang_muka,
                tanggal_jual: item.tanggal_jual,
                nama_konsumen: item.nama_konsumen,
                alamat: item.alamat,
                atas_nama: item.atas_nama,
                penjualan_id: item.penjualan_id
              }
          }));
        }
      }).done(function(){ 
        $("#no_ref").val(null);
        $("#no_rangka").val(null);
        $("#no_mesin").val(null);
        $("#no_serie").val(null);
        $("#no_stnk").val(null);
        $("#kendaraan_id").val(null);
        $("#tipe").val(null);
        $("#warna").val(null);
        $("#uang_muka").val(null);
        $("#tanggal_jual").val(null);
        $("#nama_konsumen").val(null);
        $("#alamat").val(null);
        $("#atas_nama").val(null);
        $("#penjualan_id").val(null);
        });
    },
    delay: 1000,
    minLength: 1,
    select: function( event, ui ) {
      log( ui.item ? "Selected: " + ui.item.label : "Nothing selected, input was " + this.value);
      console.log(ui)
      $("#no_ref").val(ui.item.no_ref);
      $("#no_rangka").val(ui.item.no_rangka);
      $("#no_mesin").val(ui.item.no_mesin);
      $("#no_serie").val(ui.item.no_serie);
      $("#no_stnk").val(ui.item.no_stnk);
      $("#kendaraan_id").val(ui.item.kendaraan_id);
      $("#tipe").val(ui.item.tipe);
      $("#warna").val(ui.item.warna);
      $("#uang_muka").val(ui.item.uang_muka);
      $("#tanggal_jual").val(ui.item.tanggal_jual);
      $("#nama_konsumen").val(ui.item.nama_konsumen);
      $("#alamat").val(ui.item.alamat);
      $("#atas_nama").val(ui.item.atas_nama);
      $("#penjualan_id").val(ui.item.penjualan_id);
      $(".money").each(function(){
        $(this).val(stringToNumber( numberToString( $(this).val() ) ,0));
      })
    },
    open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    },
    close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    }
  });

  $(".autocomplete_penjualan").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "http://"+window.location.hostname+"/index.php/autocomplete/no_rangka",
        dataType: "json",
        data: {
          maxRows: 12,
          term: request.term
        },
        success: function( data ) {
          response( $.map( data.result, function( item ) {
            console.log(item)
            return {
                label: item.no_rangka +" - "+item.penjualan_id,
                value: item.no_rangka
              }
          }));
        }
      }).done(function(){ 
        $("#no_mesin").val(null);
        $("#no_serie").val(null);
        $("#no_stnk").val(null);
        $("#nama_kendaraan").val(null);
        $("#kendaraan_id").val(null);
        $("#kode_tipe").val(null);
        $("#kode_warna").val(null);
        $("#dealer_id").val(null);

        $("#konsumen_id").val(null);
        $("#nama_konsumen").val(null);

        $("#stock_id").val(null);
        $("#tanggal_pembelian").val(null);
        $("#tanggal_keluar").val(null);
        $("#harga_beli").val(null);
        $("#hpp").val(null);
      });
    },
    delay: 1000,
    minLength: 1,
    select: function( event, ui ) {
      console.log(ui);
      log( ui.item ?
          "Selected: " + ui.item.label :
          "Nothing selected, input was " + this.value);
      $.ajax({
        url: "http://"+window.location.hostname+"/index.php/autocomplete/kendaraan",
        dataType: "json",
        data: {
          term: ui.item.value,
          type: "row"
        },
        success: function( data ) {
          console.log(data);
          $("#no_mesin").val(data.no_mesin);
          $("#no_serie").val(data.no_serie);
          $("#no_stnk").val(data.no_stnk);
          $("#nama_kendaraan").val(data.nama_kendaraan);
          $("#kendaraan_id").val(data.kendaraan_id);
          $("#kode_tipe").val(data.tipe_id);
          $("#kode_warna").val(data.warna_id);
          $("#dealer_id").val(data.dealer_id);

          $("#konsumen_id").val(data.konsumen_id);
          $("#nama_konsumen").val(data.nama_konsumen);

          $("#stock_id").val(data.stock_id);
          $("#tanggal_pembelian").val(data.tanggal_do);
          $("#tanggal_keluar").val(data.tanggal_keluar);
          $("#harga_beli").val(stringToNumber(data.hutang_ktb));
          countHpp();
        }
      })

    },
    open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    },
    close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    }
  });

  $(".no_rangka").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "http://"+window.location.hostname+"/index.php/autocomplete/no_rangka",
        dataType: "json",
        data: {
          maxRows: 12,
          term: request.term
        },
        success: function( data ) {
          // alert("shit");
          response( $.map( data.result, function( item ) {
            return {
                label: item.no_rangka +" - "+item.name,
                value: item.no_rangka
              }
          }));
        }
      })
    },
    delay: 1000,
    minLength: 1,
    select: function( event, ui ) {
        log( ui.item ?
            "Selected: " + ui.item.label :
            "Nothing selected, input was " + this.value);
    },
    open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    },
    close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    }
  });

  $("#no_serie").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "http://"+window.location.hostname+"/index.php/autocomplete/no_serie",
        dataType: "json",
        data: {
          maxRows: 12,
          term: request.term
        },
        success: function( data ) {
          response( $.map( data.result, function( item ) {
            return {
                label: item.no_serie +" - "+item.name,
                value: item.no_serie
              }
          }));
        }
      });
    },
    delay: 1000,
    minLength: 1,
    select: function( event, ui ) {
        log( ui.item ?
            "Selected: " + ui.item.label :
            "Nothing selected, input was " + this.value);
    },
    open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    },
    close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    }
  });

  $("#no_mesin").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "http://"+window.location.hostname+"/index.php/autocomplete/no_mesin",
        dataType: "json",
        data: {
          maxRows: 12,
          term: request.term
        },
        success: function( data ) {
          response( $.map( data.result, function( item ) {
            return {
                label: item.no_mesin +" - "+item.name,
                value: item.no_mesin
              }
          }));
        }
      });
    },
    delay: 1000,
    minLength: 1,
    select: function( event, ui ) {
        log( ui.item ?
            "Selected: " + ui.item.label :
            "Nothing selected, input was " + this.value);
    },
    open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    },
    close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    }
  });

  $("#no_stnk").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "http://"+window.location.hostname+"/index.php/autocomplete/no_stnk",
        dataType: "json",
        data: {
          maxRows: 12,
          term: request.term
        },
        success: function( data ) {
          response( $.map( data.result, function( item ) {
            return {
                label: item.no_stnk +" - "+item.name,
                value: item.no_stnk
              }
          }));
        }
      });
    },
    delay: 1000,
    minLength: 1,
    select: function( event, ui ) {
        log( ui.item ?
            "Selected: " + ui.item.label :
            "Nothing selected, input was " + this.value);
    },
    open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    },
    close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    }
  });

  $("#no_acc, .no_acc").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "http://"+window.location.hostname+"/index.php/autocomplete/perkiraan",
        dataType: "json",
        data: {
          maxRows: 12,
          term: request.term
        },
        success: function( data ) {
          response( $.map( data.result, function( item ) {
            return {
                label: item.id +" - "+item.name +" - "+item.g +" - "+item.l +" - "+item.sl,
                value: item.id
              }
          }));
        }
      });
    },
    delay: 1000,
    minLength: 1,
    select: function( event, ui ) {
        log( ui.item ?
            "Selected: " + ui.item.label :
            "Nothing selected, input was " + this.value);
    },
    open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    },
    close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    }
  });

  $(".kendaraan_tipe").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "http://"+window.location.hostname+"/index.php/autocomplete/tipe_kendaraan",
        dataType: "json",
        data: {
          maxRows: 12,
          term: request.term
        },
        success: function( data ) {
          response( $.map( data.result, function( item ) {
            return {
                label: item.id +" - "+item.name +" - "+item.jenis,
                name: item.name,
                jenis: item.jenis,
                value: item.id,
                harga: item.harga
              }
          }));
        }
      });
    },
    delay: 1000,
    minLength: 1,
    select: function( event, ui ) {
      $(this).parent().siblings(".nama_tipe").html(ui.item.name);
      $(this).parent().siblings(".jenis_tipe").html(ui.item.jenis);
      $(this).parent().siblings(".harga_tipe").children(".harga_tipe").val(ui.item.harga);
      setAllMoney("money","input")
      log( ui.item ?
        "Selected: " + ui.item.label :
        "Nothing selected, input was " + this.value);
    },
    open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    },
    close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    }
  });

  $(".kendaraan_warna").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "http://"+window.location.hostname+"/index.php/autocomplete/warna_kendaraan",
        dataType: "json",
        data: {
          maxRows: 12,
          term: request.term
        },
        success: function( data ) {
          response( $.map( data.result, function( item ) {
            return {
                label: item.id +" - "+item.nama_warna_en +" - "+item.nama_warna_id,
                name: item.nama_warna_id,
                value: item.id
              }
          }));
        }
      });
    },
    delay: 1000,
    minLength: 1,
    select: function( event, ui ) {
      $(this).parent().siblings(".nama_warna").html(ui.item.name);
      log( ui.item ?
          "Selected: " + ui.item.label :
          "Nothing selected, input was " + this.value);
    },
    open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    },
    close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    }
  });

  $(".kode_marketing").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "http://"+window.location.hostname+"/index.php/autocomplete/kode_marketing",
        dataType: "json",
        data: {
          maxRows: 12,
          term: request.term
        },
        success: function( data ) {
          // console.log(data[0].id + " "+ data[0].nama_marketing)
          response( $.map( data.result, function( item ) {
            return {
                label: item.id +" - "+item.nama_marketing,
                value: item.id
              }
          }));
        }
      });
    },
    delay: 1000,
    minLength: 1,
    select: function( event, ui ) {
        log( ui.item ?
            "Selected: " + ui.item.label :
            "Nothing selected, input was " + this.value);
    },
    open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    },
    close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    }
  });

  $(".kode_leasing").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "http://"+window.location.hostname+"/index.php/autocomplete/kode_leasing",
        dataType: "json",
        data: {
          maxRows: 12,
          term: request.term
        },
        success: function( data ) {
          response( $.map( data.result, function( item ) {
            return {
                label: item.id +" - "+item.nama_leasing,
                value: item.id
              }
          }));
        }
      });
    },
    delay: 1000,
    minLength: 1,
    select: function( event, ui ) {
        log( ui.item ?
            "Selected: " + ui.item.label :
            "Nothing selected, input was " + this.value);
    },
    open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    },
    close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    }
  });

}