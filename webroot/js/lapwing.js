var current_dealer = 0;
$(document).ready(function(){
  get_menu_after_reload();
  $("#choose_dealer").change(function(){
      current_dealer = $("#choose_dealer").val();
      $('.input_current_dealer').val(current_dealer);
  });
});

$(document).ajaxStart(function() {
  $(".warning h3").text('Loading...');
  $(".warning p").text('');
  ajaxLoadLoading("warning");
});
$(document).ajaxStop(function() {
  $('.warning').animate({"top": "-104"}, 0);
});



// NOTIFICATION FOR MESSAGE
var myMessages = ['info','warning','error','success'];
function hideAllMessages()
{
  var messagesHeights = new Array(); // this array will store height for each

  for (i=0; i<myMessages.length; i++)
  {
      messagesHeights[i] = $('.' + myMessages[i]).outerHeight(); // fill array
      $('.' + myMessages[i]).css('top', -messagesHeights[i]); //move element outside viewport
  }
}
function showMessage(type)
{
  if (type.length != 0){
      $('.'+ type).animate({"top": "0"}, 500);
      $('.'+ type +'-trigger').click(function(){
          hideAllMessages();
          $('.'+type).animate({"top": "-65"}, 500);
      });
      setTimeout(function(){
          $('.'+type).animate({"top": -$('.'+type).outerHeight()}, 500);
      },5000);
  }
}

function ajaxLoadLoading(type){
  if (type.length != 0){
      $('.'+ type).animate({"top": "-50"}, 0);
  }
}

function ajaxLoadMessage(type){
  if (type.length != 0){
      $('.'+ type).animate({"top": "0"}, 0);
      $('.'+ type +'-trigger').click(function(){
          hideAllMessages();
          $('.'+type).animate({"top": "-65"}, 0);
      });
  }
}

// END NOTIFICATION

function showMenu(url,name,id_tab){
  $.ajax({
      url: url,
      context: document.body
  }).done(function(data) { 
      $("#content").empty();
      $("#content").html(data);
          if(id_tab!=9999){
              $('#tabs').tabs({ active: id_tab });
          }
      });

  $(".ribbon > h1").html(name);
};

function get_menu_after_reload(){
  var id_tab=9999;
  var idk;
  var hash = document.URL.split('#');
  if (hash.length>1){
      var idk =document.URL.split("-");
      if (idk.length > 1){
          id_tab = idk[1];
          var tempname = hash[1].split('-');
          var name = tempname[0];
      }else{
          var name = hash[1];
      }
      if (name.length>1){
          var url = location.protocol + '//' + location.host +'/index.php/'+name;
          if (name=="stock"){
              url = location.protocol + '//' + location.host +'/index.php/home/ajax_index';
          }
          name=name.replace(/_/g,' ');
          name = name.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
          showMenu(url,name,id_tab);
      }
  }
  current_dealer = $("#choose_dealer").val();
  $('.input_current_dealer').val(current_dealer);
}

// validation for each form
function check_required(form_id){
  var status  = true;
  $("#"+form_id+" >.required").each(function(){
    if ( $(this).val().length<1) {
    $(this).css('box-shadow','0px 2px 2px rgb(204, 204, 204) inset, 0px 0px 10px rgb(229, 174, 174)');
    $(this).css('background-color','#FFEAEA');
    status = false;
  }else{
    $(this).attr('style','');
  }
  });
  return status;
}

function stringToNumber(number, decimals, dec_point, thousands_sep) {
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
      prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
      sep = (typeof thousands_sep === 'undefined') ? '.' : thousands_sep,
      dec = (typeof dec_point === 'undefined') ? ',' : dec_point,
      s = '',
      toFixedFix = function (n, prec) {
          var k = Math.pow(10, prec);
          return '' + Math.round(n * k) / k;
      };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
      s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
      s[1] = s[1] || '';
      s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

function numberToString(number, dec_point, thousands_sep ) {
  var number = String(number)
  var sep = (typeof thousands_sep === 'undefined') ? '.' : thousands_sep,
      dec = (typeof dec_point === 'undefined') ? ',' : dec_point,
      big_n=[]
  big_n.push(number.split(dec)[0].replace(/[^0-9]+/g, ""))
  number.split(dec).length != 1 ? big_n.push(number.split(dec)[1]) : '';
  return big_n.length==1 ? big_n : big_n.join('.')
}

function setAllMoney(money,type){
  if (type=="input"){
    $("."+money).each(function(){
      // console.log($(this).val())
      $(this).val( stringToNumber(numberToString($(this).val()),0) )
    })
  }else{
    $("."+money).each(function(){
      // console.log($(this).html())
      $(this).html( stringToNumber(numberToString($(this).html()),0) )
    })
  }
}

function numberOnly(field){
  $(field).keydown(function(e) {
    return ( (e.keyCode >= 48 && e.keyCode <= 57) || e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 9 || e.keyCode == 16 || e.keyCode == 116 || (e.keyCode >= 96 && e.keyCode <= 105))
  });
}

function refreshKeyUpRekap(){
  $('.money').keyup(function(e){
    if (e.keyCode!=188 && e.keyCode != 190 && e.keyCode != 110){
      var money = numberToString($(this).val());
      var money_n = stringToNumber(money,0);
      $(this).val(money_n)
    }else{
      e.preventDefault(0)
    }
  });

  numberOnly(".money")
}

function datepicker() {
  $('.datepicker').datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd-mm-yy'
  });
}

function showMessageWithText(type, message){
  $(".message").children("p").text(message);
  if(type == "success"){
    showMessage("success");
    console.log("masuk");
  }
  else{
    showMessage("error");
  }
}